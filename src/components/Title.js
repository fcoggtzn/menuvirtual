import React, { Component } from 'react'
import PropTypes from 'prop-types'


Title.propTypes = {
    title: PropTypes.string.isRequired
}


export default function Title(props) {

  

   
        return (
            <div className="card">
                  <div className="card-body">
                      {props.title}
                 </div>
            </div>
        )
    
}

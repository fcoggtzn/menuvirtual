import React, { Component } from 'react'
import baseDatos from './configuration/config'
import { Link } from 'react-router-dom';




export default class ObtenerMenu extends Component {

    getEmpresas = () => {
        let ref = baseDatos.database().ref('/').child('menu').orderByKey();
        ref.on('value', snapshot => {
          const state = snapshot.val();
          this.setState(state);
          console.log(this.state.empresas)
        });
         //  console.log('DATA RETRIEVED');
      }


    componentDidMount(){
        this.getEmpresas()
       
    }



    constructor(){
        super()
        this.state={ empresas: [],
                    }
    }

    render() {
     
        return (
            <div>
             {
             
                this.state.empresas
                .map((empresa, index) => 
                
                 <Link to={`menu/${empresa}`}  key={index}>
                  <div   className="card float-left" style={{width: '18rem', marginRight: '1rem'}}>
                  
                    <div className="card-body">
                      <h5   className="card-title">{ empresa }</h5>
                      </div>
                      
                 </div>
                 </Link>               
                 
                
                )
         }    
               
            </div>
        )
       
    }
}

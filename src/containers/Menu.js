import React, { Component } from 'react'
import baseDatos from './configuration/config'
import PropTypes from 'prop-types'





export default class Menu extends Component {
   
   static propTypes = {
        match: PropTypes.shape({
            params: PropTypes.object,
            isExact: PropTypes.bool,
            path: PropTypes.string,
            url: PropTypes.string
        })
    }

    getMenu = (empresa) => {
        let ref = baseDatos.database().ref().child('empresas').child(empresa);
        let productos = [];
        ref.on('value', snapshot => {
          snapshot.forEach(function(childSnapshot) {
            // key will be "ada" the first time and "alan" the second time
            var key = childSnapshot.key;
            // childData will be the actual contents of the child
            var childData = childSnapshot.val();
            console.log(key,childData);
            productos.push(childData);
        }); 
        this.setState({productos});  
        console.log(this.state.productos);

        });
         //  console.log('DATA RETRIEVED');
      }
      
      
   
   constructor(){    
    super()
    //const { empresa } = this.props.match.params.empresa
    this.state={ productos: []}
    
   }

    componentDidMount(){
          let  empresa = this.props.match.params.empresa
          console.log(this.props)
          console.log(empresa)
          this.getMenu(empresa)
              
    }

    render() {
   
        return (
            
            <div> 
            <h5> </h5>
             {
                this.state.productos
                .map((producto, index) => 
                  <div onClick ={ ()=>{this.setState({producto})}}  key={index} className="card float-left" style={{width: '18rem', marginRight: '1rem'}}>
                  <img src={producto.foto} className="card-img-top" alt={producto.nombre} />
                    <div className="card-body">
                      <h5   className="card-title">{ producto.precio }</h5>
                      </div>
                 </div>
                )
             }    
            </div>
        )
       
    }
}


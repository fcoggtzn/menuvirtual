import React, { Component } from 'react'
import QRCode from 'react-qr-code'



export default class QrCodeFggn extends Component {
   

    render() {
        let empresa =this.props.match.params.empresa
        return (
            <div class="container">
            <div className="card mt-2 justify-content-center" style={{width: '18rem', marginRight: '1rem'}}>
               <QRCode value={`http://cartavirtual.web.app/menu/${empresa}`}> </QRCode>
                <div className="card-body">
                    <p className="card-text">{empresa}</p>
                </div>
            </div></div>
          
        )
    }
}

import * as firebase from 'firebase'

let firebaseConfig = {
    apiKey: "AIzaSyCO2Wkk0gPHaDz-xdKEpDlTdL5s7elLrW0",
    authDomain: "menuvirtual-7db34.firebaseapp.com",
    databaseURL: "https://menuvirtual-7db34.firebaseio.com",
    projectId: "menuvirtual-7db34",
    storageBucket: "menuvirtual-7db34.appspot.com",
    messagingSenderId: "606261247943",
    appId: "1:606261247943:web:d38603055f598e18a3a3e2",
    measurementId: "G-E6Y4GHC76N"
  };
  const baseDatos = firebase.initializeApp(firebaseConfig)

  export default baseDatos;
import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Title from './components/Title';
import ObtenerMenu from './containers/ObtenerMenu';
import { Switch, Route } from 'react-router-dom';
import Menu from './containers/Menu';
import QrCodeFggn from './containers/QrCode';


function App() {

  return (
   <div className="App">
   <Title title="Menu" />
   <Switch >
   <Route exact path='/' component={ObtenerMenu}></Route>
   <Route exact path='/menu/:empresa' component={Menu}></Route>
   <Route exact path='/code/:empresa' component={QrCodeFggn}></Route>

   </Switch>
     
      
   </div>
  );
}

export default App;
